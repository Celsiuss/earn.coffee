import { EarnPage } from './app.po';

describe('earn App', () => {
  let page: EarnPage;

  beforeEach(() => {
    page = new EarnPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
