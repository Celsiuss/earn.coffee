import {Injectable, OnInit} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';
import {Observer} from "rxjs/Observer";
import {Router} from "@angular/router";

@Injectable()
export class AuthService implements OnInit{

	private observer: Observer<any>;

	userInfo = {
		fulfilled: false,
		id: null,
		username: 'Loading...',
		email: '',
		created: '',
		points: 0,
		totalPoints: '',
		disabled: false,
		verified: false,
		gravatar: '',
		role: '',
		referrals: 0,
		referralEarnings: 0
	};

	signedIn = false;

	userObservable: Observable<any>;

	constructor(private http: Http,
				private router: Router) {
		this.userObservable = new Observable(observer => {
			this.observer = observer;
			// if (this.isSignedIn()) {
			// 	this.updateUser().then(() => {
			// 		console.log('updated user')
			// 	});
			//
			// }
		});

	}

	ngOnInit() {

	}

	signUp(username: string, email: string, password: string): Observable<any> {
		let referrer = 'none';
		if (localStorage.getItem('refferer')) referrer = localStorage.getItem('referrer');
		let body = {
			name: username,
			email: email,
			password: password,
			referrer: referrer
		};
		return this.http.post(environment.url + '/auth/signup', body)
			.map((response: Response) => {return response.json()})
			.catch(err => Observable.throw(err.json()));
	}

	signIn(email: string, password: string, captcha: string): Observable<any> {
		let body = {
			email: email,
			password: password,
			captcha: captcha
		};
		return this.http.post(environment.url + '/auth/signin', body)
			.map((response: Response) => {return response.json()})
			.catch(err => Observable.throw(err.json()))
	}

	gSignIn(token: string): Observable<any> {
		let referrer = 'none';
		if (localStorage.getItem('referrer')) referrer = localStorage.getItem('referrer');
		let body = {
			token: token,
			referrer: referrer
		};
		return this.http.post(environment.url + '/auth/g-signin', body)
			.map((response: Response) => response.json())
			.catch(err => Observable.throw(err.json()))
	}

	isSignedIn(): boolean {
		if (localStorage.getItem('token')) {
			return true;
		} else return false;
	}

	logout(): void {
		this.signedIn = false;
		localStorage.clear();
		this.router.navigate(['/']);
	}

	updateUser() {
		localStorage.removeItem('referrer');
		this.userInfo.fulfilled = false;
		return new Promise((resolve, reject) => {
			let headers = new Headers();
			headers.append('Authorization', localStorage.getItem('token'));

			let options = new RequestOptions({headers: headers});
			this.http.get(environment.url + '/account/getUserData', options)
				.subscribe(
					(response: Response) => {
						console.log('signedin');
						this.signedIn = true;
						let user = response.json().obj;
						let newUser: any = {
							fulfilled: true,
							id: user._id,
							username: user.username,
							email: user.email,
							created: user.created,
							points: user.points,
							totalPoints: user.totalPoints,
							disabled: user.disabled,
							verified: user.verified,
							gravatar: user.gravatar,
							role: user.role,
							referrals: user.referrals,
							referralEarnings: user.referralEarnings
						};
						this.userInfo = newUser;
						this.observer.next(newUser);
						resolve(newUser);
					},
					error => {
						this.signedIn = false;
						if (error.json().message == 'User does not exist') localStorage.clear();
						this.router.navigate(['/'])//.then(() => location.reload());
						reject(error.json());
					}
				);
		});
	}

}
