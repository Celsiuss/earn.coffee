import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "./auth.service";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";

export interface FormModel {
    captcha?: string;
}

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
	signUpForm = new FormGroup({
		name: new FormControl(),
		email: new FormControl(),
		password: new FormControl()
	});
	alerts = [];
	waiting = false;

    captchaResponse = '';

    public formModel: FormModel = {};

	constructor(public activeModal: NgbActiveModal,
				private fb: FormBuilder,
				private authService: AuthService,
				private router: Router) {
		this.createForm();
	}

	createForm(): void {
		this.signUpForm = this.fb.group({
			name: ['', [
				Validators.required,
				Validators.minLength(4),
				Validators.maxLength(15)
			]],
			email: ['', [
				Validators.required,
				Validators.email
			]],
			password: ['', [
				Validators.required,
				Validators.minLength(6)
			]]
		});
	}

	onSubmit(): void {
		this.waiting = true;
		let body = {
			username: this.signUpForm.value.name,
			email: this.signUpForm.value.email,
			password: this.signUpForm.value.password
		};
		this.authService.signUp(body.username, body.email, body.password)
			.subscribe(
				value => {
					this.waiting = false;
					if (value.message == 'User created') {
						this.authService.signIn(body.email, body.password, this.captchaResponse)
							.subscribe(
								value => {
									localStorage.setItem('token', value.token);
									this.authService.updateUser().then(() => {
										this.activeModal.close();
										this.router.navigate(['home']);
									});
								},
								error => {
									this.waiting = false;
									this.alerts.push(error.message);
								}
							);
					}
				},
				error => {
					this.waiting = false;
					this.alerts.push(error.message)
				}
			);
	}

	closeAlert(i): void {
		this.alerts.splice(i, 1);
	}

    onCaptcha(event) {
        this.captchaResponse = event;
    }

	get name() {return this.signUpForm.get('name')}
	get email() {return this.signUpForm.get('email')}
	get password() {return this.signUpForm.get('password')}

	ngOnInit() {
	}

}
