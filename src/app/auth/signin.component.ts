import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {SignupComponent} from "./signup.component";
import {AuthService} from "./auth.service";
import {NgForm} from "@angular/forms";
import {ForgotComponent} from './forgot.component';

export interface FormModel {
	captcha?: string;
}

@Component({
	selector: 'app-signin',
	templateUrl: './signin.component.html',
	styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit, OnDestroy {
	@Input() name;
	@Input('signinForm') signinForm: NgForm;
	@ViewChild('captchaControl') captchaControl;

	public formModel: FormModel = {};

	alerts = [];
	waiting = false;

	captchaResponse = '';

	constructor (public activeModal: NgbActiveModal,
				private modalService: NgbModal,
				private auth: AuthService,
				private router: Router,
				private route: ActivatedRoute) {
	}

	ngOnInit() {
	}

	ngOnDestroy() {

	}

	signUp(): void {
		this.activeModal.close();
		const signup = this.modalService.open(SignupComponent);
		signup.componentInstance.name = 'signup';
	}

	signIn(form): void {
		this.waiting = true;
		this.auth.signIn(form.value.email, form.value.password, this.captchaResponse)
			.subscribe(
				(response) => {
					this.waiting = false;
					localStorage.setItem('token', response.token);
					this.auth.updateUser().then(() => {
						this.activeModal.close();
						this.router.navigate(['home']);
					});
				},
				error => {
					this.waiting = false;
					this.alerts.push(error.message);
					this.captchaControl.reset();
				}
			);
	}

	closeAlert(i) {
		this.alerts.splice(i, 1);
	}

	forgotPassword(): void {
		this.activeModal.close();
		const signup = this.modalService.open(ForgotComponent);
		signup.componentInstance.name = 'forgotPassword';
	}

	captcha(event) {
		this.captchaResponse = event;
	}

	logout(): void {
		// this.afAuth.auth.signOut().then( () => {
		//   console.log('logged out');
		// }).catch( (err) => {console.log(err)});
	}

}
