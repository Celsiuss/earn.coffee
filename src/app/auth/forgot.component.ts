import { Component, OnInit } from '@angular/core';
import {Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-forgot',
	templateUrl: './forgot.component.html',
	styleUrls: ['./signin.component.css']
})
export class ForgotComponent implements OnInit {
	alerts = [];

	constructor (private http: Http,
				 public activeModal: NgbActiveModal) { }

	ngOnInit() {
	}

	submit(form): void {
		let email = form.value.email;
		this.http.post(environment.url + '/auth/requestPasswordReset', {email: email})
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response: Response) => {
					this.alerts.push({type: 'success', message: response.json().message})
				},
				(error) => {
					this.alerts.push({type: 'danger', message: error.message});
				}
			);
	}

	closeAlert(i) {
		this.alerts.splice(i, 1);
	}

}
