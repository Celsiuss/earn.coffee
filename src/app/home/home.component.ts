import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {Socket} from 'ng-socket-io';
import {Router} from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
	name = '';

	constructor(private router: Router,
				public auth: AuthService,
				private cdRef: ChangeDetectorRef) { }

	ngOnInit() {
		if (!localStorage.getItem('token')) {
			this.router.navigate(['/'])
		}
	}

	ngAfterViewInit() {
		this.cdRef.detectChanges();
	}

}
