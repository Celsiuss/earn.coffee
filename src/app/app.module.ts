import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NgbActiveModal, NgbDropdown, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChatComponent } from './chat/chat.component';
import { SigninComponent } from './auth/signin.component';
import {environment} from '../environments/environment';
import {AuthService} from './auth/auth.service';
import { WelcomeComponent } from './welcome.component';
import { HomeComponent } from './home/home.component';
import {AppRouting} from './app.routes';
import {AuthGuard} from './auth/AuthGuard';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './spinner/spinner.component';
import {SharedModule} from './shared.module';
import {SignupComponent} from './auth/signup.component';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import {ChatService} from './chat/chat.service';
import {ForgotComponent} from './auth/forgot.component';
import { LinksComponent } from './navbar/links.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RECAPTCHA_SETTINGS, RecaptchaModule, RecaptchaSettings} from "ng-recaptcha";
import {RecaptchaFormsModule} from "ng-recaptcha/forms";
import {ErrorDialogComponent} from "./error-dialog.component";


const socketConfig: SocketIoConfig = {url: environment.url};

@NgModule({
	declarations: [
		AppComponent,
		NavbarComponent,
		ChatComponent,
		SigninComponent,
		SignupComponent,
		WelcomeComponent,
		HomeComponent,
		UserComponent,
		SpinnerComponent,
		ForgotComponent,
		LinksComponent,
		ErrorDialogComponent
	],
	imports: [
		BrowserModule.withServerTransition({appId: 'app-id'}),
		AppRouting,
		NgbModule.forRoot(),
		SharedModule,
		SocketIoModule.forRoot(socketConfig),
		BrowserAnimationsModule,
		RecaptchaModule.forRoot(),
		RecaptchaFormsModule
	],
	providers: [
		NgbActiveModal,
		NgbModal,
		NgbDropdown,
		AuthGuard,
		AuthService,
		ChatService,
		{
			provide: RECAPTCHA_SETTINGS,
			useValue: {siteKey: '6Ld7PzcUAAAAAL7iUA16dn7CbTd5nBRIFdNES6Sq'} as RecaptchaSettings
		}
	],
	bootstrap: [AppComponent],
	entryComponents: [
		SigninComponent,
		SignupComponent,
		ForgotComponent,
		ErrorDialogComponent
	]
})
export class AppModule {}
