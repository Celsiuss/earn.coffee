import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-faq',
	templateUrl: './faq.component.html',
	styles: []
})
export class FaqComponent implements OnInit, OnDestroy, AfterViewInit {
	@ViewChild('tabset') tabset: NgbTabset;
	private routeSub: Subscription;
	private tabSub: Subscription;

	constructor(private route: ActivatedRoute,
				private router: Router) { }

	ngOnInit() {
		this.tabset.tabChange.subscribe(
			info => {
				this.router.navigate(['help', info.nextId]);
			}
		);
	}

	ngAfterViewInit() {
		setTimeout(() => {
			this.routeSub = this.route.params.subscribe(
				params => {
					this.tabset.select(params.page);
				}
			)
		}, 1);
	}

	ngOnDestroy() {
		this.routeSub.unsubscribe();
		//this.tabSub.unsubscribe();

	}


}
