import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FaqComponent } from './faq.component';
import { NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';

const appRoutes: Routes = [
	{path: ':page', component: FaqComponent}
];

@NgModule({
	imports: [
		CommonModule,
		RouterModule.forChild(appRoutes),
		NgbTabsetModule
	],
	declarations: [
		FaqComponent
	],
    providers: [
    ]
})
export class HelpModule { }
