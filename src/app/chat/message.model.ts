export class Message {
  constructor(public senderName: string,
              public senderUid: string,
              public message: string,
              public time: number) {
  }
}
