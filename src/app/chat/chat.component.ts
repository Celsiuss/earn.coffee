import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Message} from './message.model';
import {Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {Headers} from '@angular/http';
import 'rxjs/Rx';
import {Socket} from 'ng-socket-io';
import {ChatService} from './chat.service';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @ViewChild('box') private box: ElementRef;

  user: string;
  messages = [];

  isLoading = true;

  cooldown = 0;

  constructor(private http: Http,
              private chat: ChatService,
			 public auth: AuthService) { }

  ngOnInit() {
  	this.http.get(environment.url + '/getMessages').subscribe(
		(response: Response) => {
			this.isLoading = false;
			this.messages = response.json().obj.reverse();
			this.scrollToBottom();
		}
	);

    this.chat.getMessage().subscribe(
        msg => {
			this.messages.push(msg);
			this.scrollToBottom();
        }
    );
  }

  sendMessage(form: any): void {
    if (form.value.message && this.cooldown < Date.now()) {
		this.chat.sendMessage(form.value.message);
		this.cooldown = Date.now() + 3000;
		form.resetForm();
    }
  }

  scrollToBottom(): void {
    setTimeout(() => {
		try {
			this.box.nativeElement.scrollTop = this.box.nativeElement.scrollHeight - this.box.nativeElement.clientHeight;
		} catch (err) {}
    }, 100);
  }

}
