import {Injectable, OnInit} from '@angular/core';
import {Socket} from 'ng-socket-io';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class ChatService {

	constructor(private socket: Socket) {}

	sendMessage(message: string): void {
		if (!localStorage.getItem('token')) return;

		let object = {
			message: message,
			token: localStorage.getItem('token')
		};
		this.socket.emit('message', object);
	}

	getMessage(): Observable<any> {
		return this.socket
			.fromEvent('message')
			.map(data => data)
	}

}