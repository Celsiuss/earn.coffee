import {AfterViewInit, Component, OnInit} from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {AuthService} from "./auth/auth.service";
import {environment} from "../environments/environment";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	animations: [
		trigger('sidenavState', [
			state('closed', style({
				'width': '300px',
				transform: 'translateX(-300px)'
			})),
			state('open', style({
				'width': '200px',
				transform: 'translateX(0)'
			})),
			transition('closed <=> open', animate('500ms 0ms cubic-bezier(0.23, 1, 0.32, 1)'))
		]),
		trigger('backshadowState', [
			state('closed', style({
				'background-color': 'rgba(0, 0, 0, 0)'
			})),
			state('open', style({
				'background-color': 'rgba(0, 0, 0, 0.4)'
			}))
		])
	]
})
export class AppComponent implements OnInit, AfterViewInit {
	state = 'closed';

	constructor (private router: Router,
				 private auth: AuthService,
				 private route: ActivatedRoute) {
        let script = document.createElement('script');
        script.setAttribute('src', 'https://apis.google.com/js/platform.js?onload=__gapiLoaded');
        script.setAttribute('async', '');
        script.setAttribute('defer', '');
        document.head.appendChild(script);

        let meta = document.createElement('meta');
        meta.setAttribute('name', 'google-signin-client_id');
        meta.setAttribute('content', environment.gClientId);
        document.head.appendChild(meta);
	}

	ngOnInit() {
		this.route.queryParamMap
			.map((params: Params) => params.params)
			.subscribe(params => {
				if (params.r) localStorage.setItem('referrer', params.r)
		});

		if (this.auth.isSignedIn()) this.auth.updateUser();
		this.router.events.subscribe(
			x => this.closeSidenav()
		);
	}

	toggleSidenav(): void {
		this.state == 'open' ? this.state = 'closed' : this.state = 'open';
	}

	closeSidenav(): void {
		this.state = 'closed';
	}

	ngAfterViewInit() {
		if (environment.production) {
			console.log('%cCAUTION!', 'font-size: 3rem; color: #ff2a00; text-shadow: -3px 0 black, 0 3px black, 3px 0 black, 0 -3px black;');
			console.log('%cIf you don\'t know what you\'re doing here, please quit now. If someone told you to paste something here or give them a token, it is a SCAM. They will gain access to your account if you do so.\nhttps://en.wikipedia.org/wiki/Self-XSS', 'font-size: 1.5rem');
		}
	}

}
