import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AccountRouting} from './account.routes';
import {AccountComponent} from './account.component';
import {SharedModule} from "../shared.module";
import { NgbAlertModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import {VerifyComponent} from './verify.component';
import {ResetComponent} from './reset.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { ReferralsComponent } from './referrals/referrals.component';

@NgModule({
	imports: [
		CommonModule,
		AccountRouting,
		SharedModule,
		NgbAlertModule,
		NgbTooltipModule
	],
	declarations: [
		AccountComponent,
		VerifyComponent,
		ResetComponent,
		TransactionsComponent,
		WithdrawComponent,
		ReferralsComponent
	],
	providers: [

	]
})
export class AccountModule { }
