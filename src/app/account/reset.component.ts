import {Component, Input, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {NgForm} from '@angular/forms';

@Component({
	selector: 'app-verify',
	templateUrl: './reset.component.html',
	styles: [`
        .invalid-text {
            color: #dc3545;
        }

        .invalidinput {
            border-color: #dc3545;
        }
	`]
})
export class ResetComponent implements OnInit{
	@Input('signinForm') signinForm: NgForm;
	alerts = [];

	constructor (private http: Http,
				 private route: ActivatedRoute) {}

	ngOnInit() {

	}

	submit(form): void {
		let body = {
			password: form.value.password,
			token: this.route.snapshot.params.token
		};
		this.http.post(environment.url + '/auth/resetPassword', body)
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response: Response) => {
					this.alerts.push({type: 'success', message: response.json().message})
				},
				(error) => {
					this.alerts.push({type: 'danger', message: error.message})
				}
			);
	}

	closeAlert(i) {
		this.alerts.splice(i, 1);
	}

}
