import {Component, OnInit} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../../auth/auth.service';

@Component({
	selector: 'app-transactions',
	templateUrl: './transactions.component.html',
	styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {
	isLoading: boolean = true;
	alerts = [];
	transactions = [];
	data: any;
	show: number = -1;

	headers = new Headers();

	constructor(private http: Http,
				private auth: AuthService) {
		this.headers.append('Authorization', localStorage.getItem('token'));
	}

	ngOnInit() {
		this.load();
	}

	open(i): void {
		if (this.transactions[i].type == 'data') return;
		let toRemove = this.transactions.findIndex(item => item.type == 'data');
		if (toRemove != -1) this.transactions.splice(toRemove, 1);
		if (i > toRemove && toRemove != -1) {
			i = i -1;
		}
		if (this.show == i + 1) {
			this.show = -1;
			return;
		}
		let newTransaction = JSON.parse(JSON.stringify(this.transactions[i]));
		newTransaction.type = 'data';
		this.transactions.splice(i + 1, 0, newTransaction);
		this.show = i + 1;
	}

	reload(): void {
		this.isLoading = true;
		this.load();
	}

	load(): void {
		this.http.get(environment.url + '/account/transactions', {headers: this.headers})
			.map((response: Response) => response.json().obj)
			.catch(err => Observable.throw(err.json()))
			.subscribe(
				transactions => {
					this.transactions = transactions;
					this.isLoading = false;
				},
				error => console.log(error)
			);
	}

	closeAlert(index: number): void {
		this.alerts.splice(index, 1);
	}

}
