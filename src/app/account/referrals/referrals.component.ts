import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AuthService} from "../../auth/auth.service";

@Component({
	selector: 'app-referrals',
	templateUrl: './referrals.component.html',
	styleUrls: ['./referrals.component.css']
})
export class ReferralsComponent implements OnInit {
	@ViewChild('linkbox') linkBox: ElementRef;
	referralUrl = 'https://earn.coffee?r=' + this.auth.userInfo.id;

	constructor(private auth: AuthService) { }

	ngOnInit() {
		this.auth.userObservable.subscribe(
			user => {
				this.referralUrl = 'https://earn.coffee?r=' + this.auth.userInfo.id;
			}
		);
	}

	copyText(): void {
		this.linkBox.nativeElement.select();
		document.execCommand('Copy');
	}

}
