import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import {AuthService} from "../../auth/auth.service";

@Component({
	selector: 'app-withdraw',
	templateUrl: './withdraw.component.html',
	styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
	@Input('user') user: any;
	@Output() reload: EventEmitter<any> = new EventEmitter();

	alerts = [];

	waiting: boolean = false;

	constructor (private http: Http,
				 public auth: AuthService) { }

	ngOnInit() {
	}

	onReload(): void {
		this.auth.updateUser();
	}

	withdraw(form): void {
		this.waiting = true;
		let body = {
			amount: form.value.amount,
			address: form.value.address
		};
		let headers = new Headers();
		headers.append('Authorization', localStorage.getItem('token'));

		this.http.post(environment.url + '/account/withdraw', body, {headers: headers})
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response: Response) => {
					this.waiting = false;
					this.alerts.push({type: 'success', message: response.json().message});
				},
				(error) => {
					this.waiting = false;
					this.alerts.push({type: 'danger', message: error.message});
				}
			);

	}

	closeAlert(index: number): void {
		this.alerts.splice(index, 1);
	}

}
