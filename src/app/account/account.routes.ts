import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './account.component';
import {VerifyComponent} from './verify.component';
import {ResetComponent} from './reset.component';
import {ReferralsComponent} from "./referrals/referrals.component";

const appRoutes: Routes = [
	{path: '', component: AccountComponent},
	{path: 'verify/:token', component: VerifyComponent},
	{path: 'reset/:token', component: ResetComponent},
	{path: 'referrals', component: ReferralsComponent}
];

export const AccountRouting = RouterModule.forChild(appRoutes);
