import {Component, OnInit, ViewChild} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {AuthService} from '../auth/auth.service';

// let storage = firebase.storage();

@Component({
	selector: 'app-account',
	templateUrl: './account.component.html',
	styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
	@ViewChild('f') form;
	@ViewChild('files') files;

	username: '';
	alerts = [];
	avatarRef: any;
	transactions = [];
	data: any;
	show: number = -1;
	user: any = {
		username: 'ss',
		points: 0,
		isLoading: false
	};

	headers = new Headers();


	constructor (private http: Http,
				 public auth: AuthService) {
		this.headers.append('Authorization', localStorage.getItem('token'));
	}

	ngOnInit() {
		if (!this.auth.userInfo.fulfilled) {
			this.auth.updateUser().then(() => {
				if (!this.auth.userInfo.verified) {
					this.alerts.push({type: 'danger', message: 'Email is not verified! Some actions may be unavailable.'});
				}
			});

		}
		this.user.isLoading = false;

	}

	loadUser(): void {
		this.user.isLoading = true;
		this.auth.userObservable.take(1).subscribe(
			user => {
				//console.log(user);
			}
		);
	}

	save(form): void {
		let name = form.value.displayName;

		if (name.length < 4 || name.length > 15) {
			this.alerts.push({
				type: 'danger',
				message: 'Name must be over or equal to 4 characters and under 15 characters'
			});
			return;
		}

		let body = {
			name: name
		};

		this.http.post(environment.url + '/account/updateName', body, {headers: this.headers}).subscribe(
			(response: Response) => {
				this.auth.updateUser().then(() => {
					this.alerts.push({type: 'success', message: response.json().message});
				});
			},
			(error) => {
				Observable.throw(error.json());
			}
		);

	}

	closeAlert(index: number): void {
		this.alerts.splice(index, 1);
	}

}
