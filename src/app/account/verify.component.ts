import {Component, OnInit} from '@angular/core';
import {Http, Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
	selector: 'app-verify',
	template: `
		<h4>Verify</h4>
		<p>{{status}}</p>
	`
})
export class VerifyComponent implements OnInit{
	status: string = 'Verifying email...';

	constructor (private http: Http,
				private route: ActivatedRoute) {}

	ngOnInit() {
		let token = this.route.snapshot.params.token;
		this.http.get(environment.url + '/auth/verifyEmail?t=' + token)
			.catch(err => Observable.throw(err.json()))
			.subscribe(
				(response: Response) => {
					this.status = response.json().message;
				},
				(error: any) => {
					this.status = error.message;
				}
			);
	}

}
