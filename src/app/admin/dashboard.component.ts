import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {environment} from "../../environments/environment";

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	stats = {
		users: 0,
		transactions: 0
	};
	blockchainAccount = {
		balance: 0,
		receiveAddress: ''
	};

	constructor(private http: Http) { }

	ngOnInit() {
		let headers = new Headers();
		headers.append('Authorization', localStorage.getItem('token'));
		let options = new RequestOptions({headers: headers});
		this.http.get(environment.url + '/admin/stats', options)
			.subscribe(
				(response: Response) => {
					this.stats = response.json().obj;
				}
			);
		this.http.get(environment.url + '/admin/blockchainAccount', options)
			.subscribe(
				(response: Response) => {
					this.blockchainAccount = response.json();
				}
			);
	}

}
