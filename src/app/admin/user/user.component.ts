import { Component, OnInit } from '@angular/core';
import {Headers, Http, RequestOptions} from "@angular/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute} from "@angular/router";

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

	userInfo: any = {

	};

	constructor(private http: Http,
				private route: ActivatedRoute) { }

	ngOnInit() {
		let headers = new Headers();
		headers.append('Authorization', localStorage.getItem('token'));
		let options = new RequestOptions({headers: headers});
		this.http.post(environment.url + '/admin/userInfo', {userId: this.route.snapshot.params.id}, options)
			.map(response => response.json())
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response) => {
					console.log(response);
					this.userInfo = response.obj;
				},
				error => console.log(error)
			);
	}

	submit(form): void {
		console.log(form.value);

		let headers = new Headers();
		headers.append('Authorization', localStorage.getItem('token'));
		let options = new RequestOptions({headers: headers});
		let body = {
			userId: this.route.snapshot.params.id,
			username: form.value.username,
			email: form.value.email,
			role: form.value.role,
			verified: form.value.verified,
		};
		this.http.post(environment.url + '/admin/updateUserInfo', body, options)
			.map(response => response.json())
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response) => {
					console.log(response);

				},
				error => console.log(error)
			);

	}

	mute(): void {
		let headers = new Headers();
		headers.append('Authorization', localStorage.getItem('token'));
		let options = new RequestOptions({headers: headers});
		this.http.post(environment.url + '/admin/muteUser', {userId: this.route.snapshot.params.id}, options)
			.map(response => response.json())
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response) => {
					console.log(response);
					this.userInfo.muted = !this.userInfo.muted;
				},
				error => console.log(error)
			);
	}

	disable(): void {
		let headers = new Headers();
		headers.append('Authorization', localStorage.getItem('token'));
		let options = new RequestOptions({headers: headers});
		this.http.post(environment.url + '/admin/disableUser', {userId: this.route.snapshot.params.id}, options)
			.map(response => response.json())
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response) => {
					console.log(response);
					this.userInfo.disabled = !this.userInfo.disabled;
				},
				error => console.log(error)
			);
	}

	transaction(form): void {
		let headers = new Headers();
		headers.append('Authorization', localStorage.getItem('token'));
		let options = new RequestOptions({headers: headers});
		let body = {
			userId: this.route.snapshot.params.id,
			amount: form.value.amount,
			from: form.value.from,
		};
		this.http.post(environment.url + '/admin/newTransaction', body, options)
			.map(response => response.json())
			.catch(error => Observable.throw(error.json()))
			.subscribe(
				(response) => {
					console.log(response);

				},
				error => console.log(error)
			);
	}

}
