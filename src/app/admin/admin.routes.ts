import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {UserComponent} from "./user/user.component";

const routes: Routes = [
	{path: '', component: DashboardComponent},
	{path: 'user/:id', component: UserComponent}
];

export const AdminRouting = RouterModule.forChild(routes);
