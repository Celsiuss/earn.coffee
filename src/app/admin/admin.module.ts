import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import {AdminRouting} from "./admin.routes";
import { UserComponent } from './user/user.component';
import {SharedModule} from "../shared.module";

@NgModule({
	imports: [
		CommonModule,
		AdminRouting,
		SharedModule
	],
	declarations: [DashboardComponent, UserComponent]
})
export class AdminModule { }
