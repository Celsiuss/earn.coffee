export class User {
  constructor(public name: string,
              public email: string,
              public imageUrl: string,
              public balance: number) {}
}
