import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {environment} from "../../environments/environment";
import {AuthService} from "../auth/auth.service";
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    isLoaded: boolean = false;
    details = {
        name: 'Loading...',
        avatar: '',
        points: 0,
        bio: ''
    };

    constructor(private route: ActivatedRoute,
                private router: Router,
                private http: Http,
				public auth: AuthService) {}

    ngOnInit() {
        if (this.route.snapshot.params.id) {
            this.route.params.subscribe(
                value => {
					this.http.get(environment.url + '/user/' + this.route.snapshot.params.id)
						.subscribe(
							(response: Response) => {
								let user = response.json().obj;

								this.details.name = user.username;
								this.details.avatar = user.gravatar + '&s=400';
								this.details.points = user.totalPoints;
								if (user.bio) this.details.bio = user.bio;

								this.isLoaded = true;
							},
							(error) => {
								this.details.name = 'error';
								this.isLoaded = true;
							}
						);
                }
            );
        } else {
            let id = this.auth.userInfo.id;
            this.router.navigate(['/user', id]);
        }
    }

}
