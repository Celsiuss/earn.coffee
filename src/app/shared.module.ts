import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from "../environments/environment";
import {HttpModule} from "@angular/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
	imports: [
		CommonModule,
		HttpModule,
		FormsModule,
		ReactiveFormsModule
	],
	exports: [
		FormsModule,
		ReactiveFormsModule,
		CommonModule
	],
	declarations: []
})
export class SharedModule { }
