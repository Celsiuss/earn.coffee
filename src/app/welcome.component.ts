import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {SigninComponent} from "./auth/signin.component";
import {SignupComponent} from "./auth/signup.component";
import {environment} from '../environments/environment';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {ErrorDialogComponent} from "./error-dialog.component";
import {AuthService} from "./auth/auth.service";

declare let gapi: any;

@Component({
	selector: 'app-welcome',
	templateUrl: './welcome.component.html',
	styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, AfterViewInit {
	@ViewChild('gbutton') gButton: ElementRef;
	@ViewChild('errorModal') modal: ElementRef;
	user: any;
	disabled: boolean = false;
	auth2: any;

	constructor(private router: Router,
				private modalService: NgbModal,
				public activeModal: NgbActiveModal,
				private http: Http,
				private auth: AuthService,
				private cdRef: ChangeDetectorRef) {
		window['__gapiLoaded'] = (ev) => {
			console.log('gapi loaded');
			this.loadGapi();
		};

	}

	ngOnInit() {
		this.disabled = true;
		if (localStorage.getItem('token')) {
			return this.router.navigate(['home'])
		}
		if (typeof gapi != "undefined") {
			console.log('loading gapi');
			this.loadGapi();
		}
	}

	ngAfterViewInit() {

	}

	onSuccess(): void {

	}

	openSignin(): void {
		this.modalService.open(SigninComponent);
	}

	openSignup(): void {
		this.modalService.open(SignupComponent);
	}

	loadGapi(): void {
		gapi.load('auth2', () => {
			this.disabled = false;
			this.auth2 = gapi.auth2.init({
				client_id: environment.gClientId,
				cookiepolicy: 'single_host_origin'
			});
			this.attachGapi();
		})
	}

	attachGapi(): void {
		console.log(this.disabled);
		this.cdRef.detectChanges();
		this.auth2.attachClickHandler(document.getElementById('g-signin'), {},
			googleUser => {
				this.auth.gSignIn(googleUser.getAuthResponse().id_token)
					.subscribe(
						response => {
							localStorage.setItem('token', response.token);
							localStorage.setItem('account', 'google');
							this.auth.updateUser().then(() => {
								document.getElementsByTagName('app-welcome')[0].remove(); //TODO find better solution than this
								this.router.navigate(['home']);
							});
						},
						error => {
							let modal = this.modalService.open(ErrorDialogComponent);
							modal.componentInstance.message = error.message;
						}
					);
			},
			error => {

			});
	}

}
