import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome.component';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from './auth/AuthGuard';
import {UserComponent} from './user/user.component';

export const  appRoutes: Routes = [
	{path: '', component: WelcomeComponent},
	{path: 'user', component: UserComponent},
	{path: 'user/:id', component: UserComponent},
	{path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
	{path: 'account', loadChildren: './account/account.module#AccountModule', canActivate: [AuthGuard]},
	{path: 'help', loadChildren: './help/help.module#HelpModule'},
	{path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
	{path: 'wall', loadChildren: './wall/wall.module#WallModule'},
	{path: '**', redirectTo: '/'}
];

export const AppRouting = RouterModule.forRoot(appRoutes);
