import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WallComponent } from './wall.component';
import {WallRouting} from './wall.routes';

@NgModule({
    imports: [
        CommonModule,
        WallRouting
    ],
    declarations: [WallComponent]
})
export class WallModule { }
