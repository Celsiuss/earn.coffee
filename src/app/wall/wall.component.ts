import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-wall',
    templateUrl: './wall.component.html',
    styleUrls: ['./wall.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class WallComponent implements OnInit {
    url: any;

    constructor(private route: ActivatedRoute,
                private auth: AuthService,
                private router: Router,
                private sanitazer: DomSanitizer) { }

    ngOnInit() {
        switch (this.route.snapshot.params.wall) {
            case 'adworkmedia':
                this.url = this.sanitazer.bypassSecurityTrustResourceUrl('https://lockwall.xyz/wall/3uR/' + this.auth.userInfo.id);
                break;
            case 'kiwiwall':
                this.url = this.sanitazer.bypassSecurityTrustResourceUrl('https://www.kiwiwall.com/wall/s8BkQbHv4TjHxcEjO3FjxiScJLtnjiDb/' + this.auth.userInfo.id);
                break;
            case 'offerdaddy':
                this.url = this.sanitazer.bypassSecurityTrustResourceUrl('https://www.offerdaddy.com/wall/25593/' + this.auth.userInfo.id);
                break;
            case 'adgatemedia':
                this.url = this.sanitazer.bypassSecurityTrustResourceUrl('https://wall.adgaterewards.com/namUsA/' + this.auth.userInfo.id);
                break;
			case 'adscendmedia':
				this.url = this.sanitazer.bypassSecurityTrustResourceUrl('https://asmwall.com/adwall/publisher/113113/profile/12011?subid1=' + this.auth.userInfo.id);
				break;
            default:
                this.router.navigate(['/']);

        }
    }

}
