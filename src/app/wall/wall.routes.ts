import {RouterModule, Routes} from '@angular/router';
import {WallComponent} from './wall.component';

const routes: Routes = [
    {path: ':wall', component: WallComponent}
];

export const WallRouting = RouterModule.forChild(routes);
