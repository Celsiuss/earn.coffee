import {Component, Input} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: 'app-error-dialog',
	template: `
        <div class="modal-header">
            <h5 class="modal-title">Error</h5>
        </div>
        <div class="modal-body">
            {{message}}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-outline-dark" data-dismiss="modal" (click)="activeModal.close()">Close</button>
        </div>
	`
})
export class ErrorDialogComponent {
	@Input('message') message: string;

	constructor(public activeModal: NgbActiveModal) {}
}
