import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {NgbDropdownConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SigninComponent} from '../auth/signin.component';
import {AuthService} from "../auth/auth.service";
import {Socket} from 'ng-socket-io';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
	@Output() sidenav: EventEmitter<any> = new EventEmitter();
	isCollapsed: boolean;

	user: any;

	points = 0;

	constructor(public auth: AuthService,
				private config: NgbDropdownConfig,
				private socket: Socket) {
		config.placement = 'bottom-right';
	}

	ngOnInit() {
		this.isCollapsed = true;

		//this.points = this.auth.userInfo.points;

		this.socket.on('points', event => {
			if (event.id == this.auth.userInfo.id) {
				this.points = event.points;
			}
		});

		this.auth.userObservable.subscribe(
			user => {
				this.points = user.points;
			}
		);
	}

	// points(): Observable<any> {
	// 	return this.socket
	// 		.fromEvent(this.auth.userInfo.id)
	// 		.map(data => data)
	// }

	openSidenav(): void {
		this.sidenav.emit('open');
	}

	logout(): void {
		this.auth.logout();
	}

}
