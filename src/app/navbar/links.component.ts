import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
	selector: 'app-links',
	templateUrl: './links.component.html'
})
export class LinksComponent implements OnInit {
	@ViewChild('tmp') tmp: any;
	@Input('sidenav') sidenav = false;

	constructor(public auth: AuthService) { }

	ngOnInit() {
	}

}
